const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express()

const productRoutes = require('./routes/products')
const orderRoutes = require('./routes/orders')

mongoose.connect('mongodb://' + process.env.MONGO_ATLAS_USR + ':' + process.env.MONGO_ATLAS_PW + '@node-rest-shop-shard-00-00-tiz0w.mongodb.net:27017,node-rest-shop-shard-00-01-tiz0w.mongodb.net:27017,node-rest-shop-shard-00-02-tiz0w.mongodb.net:27017/test?ssl=true&replicaSet=node-rest-shop-shard-0&authSource=admin')

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use('/uploads', express.static('uploads'))
app.use((req, res, next) => {
    req.header("Access-Control-Allow-Origin", "*")
    req.header(
        "Access-Control-Allow-Headers", 
        "Content-Type, Authorization, Origin, X-Requested-With"
    )
    if(req.method === 'OPTIONS') {
        req.header(
            "Access-Control-Allow-Methods", 
            "PUT, PATCH, GET, DELETE, POST"
        )
        return res.status(200).json({})
    }
    next()
})

app.use('/products', productRoutes)
app.use('/orders', orderRoutes)

app.use((req, res, next) => {
    const error = new Error('Page Not found')
    error.status = 404
    next(error)
})

app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error: {
            message: error.message
        }
    })
})


module.exports = app