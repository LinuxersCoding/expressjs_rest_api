const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const multer = require('multer')

const Product = require('../models/products')

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/')
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + file.originalname)
    }
})

const fileFilter = (req, file, cb) => {
    if(file.mimetype === 'image/png') {
        cb(null, true)
    } else {
        cb(new Error('Image format must png'), false)
    }
}

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5,
    },
    fileFilter: fileFilter
})

router.get('/', (req, res, next) => {
    Product
        .find()
        .select('name price image')
        .exec()
        .then(result => {
            res.status(200).json({
                status: true,
                message: "success",
                data: result
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err.message
            })
        })
})

router.post('/', upload.single('image'), (req, res, next) => {
    const data = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        image: req.file.path
    })
    data.save()
        .then(result => {
            console.log('data has been created');
            res.status(201).json({
                status: true,
                message: "Data has been created"
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err.message
            })
        });
})

router.get('/:productId', (req, res, next) => {
    Product
        .findById(req.params.productId)
        .select('image name price')
        .exec()
        .then(result => {
            console.log(result)
            res.status(200).json({
                status: true,
                message: "success",
                data: result
            })
        })
        .catch(err => {
            console.log(err)
            res.status(400).json({
                status: false,
                message: err.message
            })
        })
    
})

router.patch('/:productId', (req, res, next) => {
    Product.update({ _id: req.params.productId}, {$set: {name: req.body.name,price: req.body.price}})
        .exec()
        .then(result => {   
            res.status(201).json({
                status: true,
                message: "Data has been updated"
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err.message
            })
        })
})

router.delete('/:productId', (req, res, next) => {
    Product.deleteOne({ _id: req.params.productId})
        .exec()
        .then(result => {
            res.status(200).json({
                status: true,
                message: 'Data has been deleted!'
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err.message
            })
        })
})

module.exports = router