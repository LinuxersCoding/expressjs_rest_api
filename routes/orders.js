const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

const Order = require('../models/orders')
const Product = require('../models/products')

router.get('/', (req, res, next) => {
    Order
        .find()
        .select('_id product quantity')
        .populate('product', ['name', 'price'])
        .exec()
        .then(result => {
            res.status(200).json({
                status: true,
                data: result
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err.message
            })
        })
})

router.post('/', (req, res, next) => {
    Product.findById(req.body.product)
        .exec()
        .then(product => {
            if(!product) {
                console.log('not found')
                res.status(400).json({
                    status: false,
                    message: 'Product not found!'
                })
            }

            const data = new Order({
                _id: new mongoose.Types.ObjectId,
                product: req.body.product,
                quantity: req.body.quantity
            })

            data
                .save()
                .then(result => {
                    return res.status(200).json({
                        status: true,
                        message: 'Order has been created!',
                    })
                })
                .catch(err => {
                    return res.status(400).json({
                        status: false,
                        message: err.message
                    })
                })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: err.message
            })
        })
        
})

router.get('/:orderId', (req, res, next) => {
    Order.findById(req.params.orderId)
        .exec()
        .then(result => {
            res.status(200).json({
                status: true,
                data: result
            })
        })
        .catch(err => {
            res.status(400).json({
                status: false,
                message: 'Order not found'
            })
        })
})

router.delete('/:orderId', (req, res, next) => {
    Order.remove({ _id: req.params.orderId})
        .exec()
        .then(result => {
            res.status(200).json({
                status: true,
                message: "Data has been deleted!"
            })
        })
        .catch(err => {
            res.status(200).json({
                status: false,
                message: err.message
            })
        })
})

module.exports = router